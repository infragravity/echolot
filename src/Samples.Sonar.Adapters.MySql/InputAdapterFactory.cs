using System.Configuration;
using Microsoft.Extensions.Logging;
using Infragravity.Sonar.Abstractions;
namespace Samples.Sonar.Adapters.MySql
{
    public sealed class AdapterFactory: IInputAdapterFactory
    {
        public IInputAdapter Create(ConnectionStringSettings connectionSettings, ConfigurationElement serverConfig, ILoggerFactory loggerFactory)
        {
            return new InputAdapter(connectionSettings);
        }
        public bool SupportsConnectionPooling
        {
            get{return true;}
        }

        public void Dispose() {}
    }
}