repo=infragravity
image=sample-mysql
label=dev
docker rmi $repo/$image:$label --force
rm -r out
#dotnet build /t:Clean,Build
#dotnet publish -c Debug -o out
dotnet publish -c Release -o out
docker build -t $repo/$image:$label .
rm -r out
#docker run -it infragravity/sample-mysql --name sonar-samples-mysql
