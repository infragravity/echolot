# Monitoring MySQL

## Overview

This example shows how to monitor MySQL on Kubernetes with Echolot and send telemetry to Prometheus, Akumuli or InfluxDb.

## Prerequisites

1. Helm v2.10.0 (or higher).

2. Prometheus v2.0 (or higher).

3. Kubernetes v1.10 (or higher).

## Build

This step is optional and applies only when if you need to build your own Echolot container for MySql. After cloning repository, run the following command to build local version of container:

```bash
./build.sh
```

## Deployment

### Add chart repository to Helm

```bash
helm repo add infragravity https://infragravity.gitlab.io/charts
helm repo update
```

### Deploy MySQL

```bash
helm install stable/mysql --name mysql --set mysqlRootPassword=Pass@word1,persistence.enabled=true,persistence.size=1Gi
```

### Update configuration

This step is optional when deployment is planned to non-default k8s namespace.

* Sonar.config
* mysql-basic-sonar.config

### Deploy Echolot to Kubernetes

Deploy Echolot using Helm chart using deployment script (deploy.sh) or by running the following command:

```bash
helm install infragravity/sonar \
    --name sonar-mysql \
    --namespace default \
    --set-file config.settings=./Sonar.config \
    --set-file config.customArrays.mysql-basic=./mysql-basic.sonar.config \
    --set image.tag=dev \
    --set image.repo=infragravity/sample-mysql
```
