#install helm v2.10.0 or higher
#$helm repo add infragravity https://infragravity.gitlab.io/charts
#$helm repo update
#install mysql
#$helm install stable/mysql --name mysql --set mysqlRootPassword=Pass@word1,persistence.enabled=true,persistence.size=1Gi
helm install infragravity/sonar \
    --name sonar-mysql \
    --namespace default \
    --set-file config.settings=./Sonar.config \
    --set-file config.customArrays.mysql-basic=./mysql-basic.sonar.config \
    --set image.tag=dev \
    --set image.repo=infragravity/sample-mysql
# cofigure secrets(optional) 
#   --set-file config.secrets=./stable/sonar/Sonar.dll.config \