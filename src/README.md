# Overview

This directory contains set of samples for monitoring data using Echolot deployed as sidecar with Docker and Kubernets.

* mysql - gathering performance metrics or any relational data from MySQL database server.
* odpnet - gathering relational data from Oracle database using ODP.NET
