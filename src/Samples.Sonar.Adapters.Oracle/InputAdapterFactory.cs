using System.Configuration;
using Microsoft.Extensions.Logging;
using Infragravity.Sonar.Abstractions;
namespace Samples.Sonar.Adapters.Oracle
{
    public sealed class AdapterFactory: IInputAdapterFactory
    {
        public IInputAdapter Create(ConnectionStringSettings connectionSettings, ConfigurationElement serverConfig, ILoggerFactory loggerFactory)
        {
            return new InputAdapter(connectionSettings, loggerFactory);
        }
        public bool SupportsConnectionPooling
        {
            get{return true;}
        }

        public void Dispose() {}
    }
}